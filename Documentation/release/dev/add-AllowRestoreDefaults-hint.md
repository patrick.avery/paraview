## Add AllowRestoreDefaults to pqScalarValueListPropertyWidget

A new hint has been added to pqScalarValueListPropertyWidget, `AllowRestoreDefaults`.
When this hint is added, one can reset a scalar value list property to default value by pressing the new button.
